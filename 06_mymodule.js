var fs = require ('fs');
var path = require('path');

module.exports = function (dir, ext, callback) {
  var filesArray = [];

  fs.readdir(dir, function (err, list) {
    if (err) {
      return callback(err);
    } else {
      list.forEach(function (file) {
        if (path.extname(file) === '.' + ext) {
          filesArray.push(file);
        }
      });
    callback(null, filesArray);
    }
  });   
}

// var fs = require('fs')
// var path = require('path')

// module.exports = function (dir, filterStr, callback) {

//   fs.readdir(dir, function (err, list) {
//     if (err)
//       return callback(err)

//     list = list.filter(function (file) {
//       return path.extname(file) === '.' + filterStr
//     })

//     callback(null, list)
//   })
// }